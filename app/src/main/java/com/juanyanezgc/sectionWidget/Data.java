package com.juanyanezgc.sectionWidget;

/**
 * Created by juanyanezgc on 04/07/2014.
 */
public class Data {
    private int week;
    private String text;

    public Data(int week, String text) {
        this.week = week;
        this.text = text;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
