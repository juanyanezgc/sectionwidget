package com.juanyanezgc.sectionWidget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by juanyanezgc on 07/07/2014.
 */
public class SectionFooterView extends LinearLayout {
    private TextView mSectionTextView;
    private View mSectionIndicator;


    public SectionFooterView(Context context) {
        super(context);
        init();
    }

    public SectionFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SectionFooterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.section_footer, this, true);
        mSectionTextView = (TextView) findViewById(R.id.txtSection);
        mSectionIndicator = findViewById(R.id.sectionIndicator);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if (selected) {
            mSectionTextView.setTypeface(null, Typeface.BOLD);
            mSectionIndicator.setVisibility(VISIBLE);
        } else {
            mSectionTextView.setText(getResources().getString(R.string.section_name, (Integer) getTag()));
            mSectionTextView.setTypeface(null, Typeface.NORMAL);
            mSectionIndicator.setVisibility(View.INVISIBLE);
        }
    }

    public void setText(int position) {
        mSectionTextView.setText(getResources().getString(R.string.section_name, position));
    }
}
