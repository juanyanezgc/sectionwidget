package com.juanyanezgc.sectionWidget;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by juanyanezgc on 04/07/2014.
 */
public class MainActivity extends Activity implements StickyListHeadersListView.OnStickyHeaderChangedListener, TabHost.TabContentFactory, TabHost.OnTabChangeListener {
    private enum SmoothScrollingMove {
        NONE, UP, DOWN
    }

    private static final int SCROLL_HACK_PIXELS = -5;


    private View.OnTouchListener mTabContentListOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mSmoothScrollingState = SmoothScrollingMove.NONE;
            mTabs.setOnTabChangedListener(null);
            mTabs.setCurrentTab(mTemporalPosition);
            mTabs.setOnTabChangedListener(MainActivity.this);
            updateSelectedSectionTab();
            mItemsList.setOnTouchListener(null);

            return false;
        }
    };

    private StickyListHeadersListView mItemsList;
    private SectionListAdapter mItemsListAdapter;
    private TabHost mTabs;
    private View mTabContentLayout;
    private HorizontalScrollView mSectionsScrollView;
    private TextView mSectionHeaderTextView;

    private View mCurrentTab;
    private int mCurrentSectionIndex;
    private int mTemporalPosition;
    private SmoothScrollingMove mSmoothScrollingState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] countries = getResources().getStringArray(R.array.countries);
        List<Data> data = new ArrayList<Data>();

        for (int i = 0; i < countries.length; i++) {
            data.add(new Data(i, countries[i]));
        }

        mItemsListAdapter = new SectionListAdapter(this, data);

        mTabContentLayout = View.inflate(this, R.layout.tab_content_layout, null);
        mSectionHeaderTextView = (TextView) mTabContentLayout.findViewById(R.id.txtSectionHeader);
        mItemsList = (StickyListHeadersListView) mTabContentLayout.findViewById(R.id.listView);
        mItemsList.setOnStickyHeaderChangedListener(this);
        mItemsList.setAdapter(mItemsListAdapter);

        mSectionsScrollView = (HorizontalScrollView) findViewById(R.id.scrollView);
        mSectionsScrollView.setSmoothScrollingEnabled(true);

        mTabs = (TabHost) findViewById(R.id.tabhost);
        mTabs.setup();


        for (int i = 0; i < mItemsListAdapter.getSections().length; i++) {
            mTabs.addTab(mTabs.newTabSpec(String.valueOf(i)).setIndicator(buildTabView(i)).setContent(this));
        }


        mCurrentTab = mTabs.getCurrentTabView();
        mCurrentTab.setSelected(true);
        mSectionHeaderTextView.setText(getString(R.string.section_header_text, mCurrentSectionIndex));
        mTabs.setOnTabChangedListener(this);

    }

    private View buildTabView(int position) {
        SectionFooterView view = new SectionFooterView(this);
        view.setTag(position);
        view.setText(position);
        return view;
    }

    @Override
    public void onStickyHeaderChanged(StickyListHeadersListView stickyListHeadersListView, View view, int position, long l) {
        if (mSmoothScrollingState == SmoothScrollingMove.NONE) {
            mTabs.setOnTabChangedListener(null);
            mTabs.setCurrentTab(mItemsListAdapter.getSectionForPosition(position));
            mTabs.setOnTabChangedListener(this);
            updateSelectedSectionTab();
        } else {
            mTemporalPosition = mItemsListAdapter.getSectionForPosition(position);
            if (mCurrentSectionIndex == mTemporalPosition) {
                mSmoothScrollingState = SmoothScrollingMove.NONE;
                mItemsList.setOnTouchListener(null);
            }
        }
    }

    @Override
    public void onTabChanged(String tag) {
        int nextSection = mTabs.getCurrentTab();
        mSmoothScrollingState = nextSection > mCurrentSectionIndex ? SmoothScrollingMove.UP : SmoothScrollingMove.DOWN;
        updateSelectedSectionTab();
        mItemsList.setOnTouchListener(mTabContentListOnTouchListener);
        int sectionFirstItemPosition = mItemsListAdapter.getPositionForSection(mCurrentSectionIndex);
        if (mSmoothScrollingState == SmoothScrollingMove.UP) {
            mItemsList.smoothScrollToPositionFromTop(sectionFirstItemPosition, SCROLL_HACK_PIXELS);
        } else {
            mItemsList.smoothScrollToPosition(sectionFirstItemPosition);
        }

    }


    @Override
    public View createTabContent(String s) {
        return mTabContentLayout;
    }

    private void updateSelectedSectionTab() {
        mCurrentTab.setSelected(false);
        mCurrentTab = mTabs.getCurrentTabView();
        mCurrentTab.setSelected(true);
        mCurrentSectionIndex = mTabs.getCurrentTab();

        int screenWidth = getWindow().getDecorView().getWidth();
        View v = mTabs.getCurrentTabView();
        if (null != v) {
            int left = v.getLeft();
            int center = screenWidth / 2 - v.getWidth() / 2;
            int scrollTo = left - center;
            mSectionsScrollView.smoothScrollTo(scrollTo, 0);
        }

        mSectionHeaderTextView.setText(getString(R.string.section_header_text, mCurrentSectionIndex));
    }

}
